import time
from MilightApi import MilightCommands, MilightHelpers, MilightSession


class MilightController:

    session = None

    def __init__(self, ip, port=5987):
        self.session = MilightSession.MilightSession(ip, port)

    def set_group(self, group):
        self.session.set_group(group)

    def turn_off(self):
        response = self.session.send_command(MilightCommands.LIGHT_OFF)

    def turn_on(self):
        response = self.session.send_command(MilightCommands.LIGHT_ON)

    def nightlight_on(self):
        response = self.session.send_command(MilightCommands.LIGHT_NIGHTLIGHT)

    def set_white(self):
        response = self.session.send_command(MilightCommands.SET_WHITE)

    def set_color_rgb(self, r, g, b):
        color_command = MilightCommands.COLOR_SET
        saturation_command = MilightCommands.SATURATION_SET

        h, s, v = MilightHelpers.rgb_to_hsv(r, g, b)

        hue = MilightHelpers.hue_to_milight(h)
        hue_hex = MilightHelpers.digit_to_hex(hue)

        saturation = MilightHelpers.digit_to_hex(s * 100)

        color_command = color_command.replace('{c}', hue_hex)
        saturation_command = saturation_command.replace('{s}', saturation)

        response = self.session.send_command(color_command)
        time.sleep(.05)
        response = self.session.send_command(saturation_command)

    def set_brightness(self, brightness):

        brightness_command = MilightCommands.BRIGHTNESS_SET

        brightness = MilightHelpers.digit_to_hex(brightness)

        brightness_command = brightness_command.replace('{b}', brightness)

        response = self.session.send_command(brightness_command)
