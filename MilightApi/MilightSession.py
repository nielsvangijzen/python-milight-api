from MilightApi import MilightCommands
import socket


class MilightSession:

    active_group = 0
    session_id = []
    sequence = 1
    target = None
    port = None
    ip = None

    def __init__(self, ip, port=5987):

        print("Creating UDP Socket")
        self.socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM
        )

        self.socket.settimeout(1)

        self.ip = ip
        self.port = port

        self.create_session()

    def send_command(self, cmd):
        header_bytes = bytearray.fromhex(MilightCommands.COMMAND_PREPEND)
        command_bytes = bytearray.fromhex(cmd)
        session_bytes = bytes(self.session_id)
        group_bytes = bytes([self.active_group])
        padding_bytes = bytearray.fromhex(MilightCommands.COMMAND_PADDING)

        cmd_bytes = bytearray()
        cmd_bytes.extend(session_bytes)
        cmd_bytes.extend(padding_bytes)
        cmd_bytes.append(self.sequence)
        cmd_bytes.extend(padding_bytes)
        cmd_bytes.extend(command_bytes)
        cmd_bytes.extend(group_bytes)
        cmd_bytes.extend(padding_bytes)

        length = len(cmd_bytes) + 1

        full_bytes = bytearray()
        full_bytes.extend(header_bytes)
        full_bytes.append(length)
        full_bytes.extend(cmd_bytes)

        checksum = self.calculate_checksum(full_bytes)
        full_bytes.append(checksum)

        self.send_data(full_bytes.hex())

        self.increment_sequence()

    def increment_sequence(self):
        if self.sequence is 255:
            self.sequence = 0
        else:
            self.sequence += 1

    def calculate_checksum(self, command):
        total = 0
        for i in range(len(command) - 11, len(command)):
            total += command[i]

        return total & 0xFF

    def send_data(self, hex_string, await_response=False):
        byte_array = bytearray.fromhex(hex_string)

        self.socket.sendto(byte_array, (self.ip, self.port))

        if await_response:
            try:
                response, source = self.socket.recvfrom(1024)
            except socket.timeout:
                response = False
        else:
            return True

        return response

    def create_session(self):
        print("Creating Session")
        print("Sending Session Request")
        send_command = MilightCommands.SESSION_START
        response = self.send_data(send_command, True)

        print("Got Session variables")
        self.session_id.append(response[19])
        self.session_id.append(response[20])

        print("Session_1 " + str(self.session_id[0]))
        print("Session_2 " + str(self.session_id[1]))

    def set_group(self, group_number):
        self.active_group = group_number
