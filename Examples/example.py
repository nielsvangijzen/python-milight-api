import MilightApi.MilightController as MilightController


# Change Accordingly
WIFI_BRIDGE_IP = '192.168.0.116'


def main():

    # Create a controller object that starts a session (Session won't change)
    # Argument0: WIFI_BRIDGE_IP_ADRESS: String
    # Argument1: WIFI_BRIDGE_PORT: Int
    milight_controller = MilightController.MilightController(WIFI_BRIDGE_IP)

    # The group is 0 (All configured groups) by default and can be changed
    # Possible values: 0 1 2 3 4
    # Argument0: WIFI_BRIDGE_GROUP: Int
    milight_controller.set_group(4)

    # Nightlight on active group
    # Just like 'On' and 'Off', is 'Nightlight' a state and can
    # Be reverted by changed by calling turn_on and turn_off
    milight_controller.nightlight_on()

    # Turn off the active group
    milight_controller.turn_off()

    # Turn on the active group
    milight_controller.turn_on()

    # Change the color of the current group, RGB values are automatically
    # Converted to HSV values and then converted to milight values
    # Argument0: Red   0-255
    # Argument1: Green 0-255
    # Argument2: Blue  0-255
    milight_controller.set_color_rgb(120, 120, 120)

    # Change the brightness of the active group to the percentage given
    # Argument0: Brightness 0-100
    milight_controller.set_brightness(100)

    # Set the active group to pure white light (Can't be done with RGB values)
    # Only available on RGBW lamps
    milight_controller.set_white()


if __name__ == "__main__":
    main()
